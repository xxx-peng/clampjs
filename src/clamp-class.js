import { debounce } from 'lodash-es'
import { addListener, removeListener } from 'resize-detector'

class Clamp {
  constructor (el, options) {
    this.options = {
      clamp: 2,
      useNative: false,
      suffixHTML: null,
      responsive: false,
      collapse: false,
      collapseBeforeSuffixHTML: '',
      collapseAfterSuffixHTML: '',
      suffixClick: () => {}
    }
    Object.assign(this.options, options)
    this.el = el
    this.collapse = false
    this.clampValue = this.options.clamp
    this.isFixedHeight = this.clampValue.indexOf && (this.clampValue.indexOf('px') > -1 || this.clampValue.indexOf('em') > -1)
    this.node = this.el.childNodes.item(0)
    this.original = this.node ? this.node.nodeValue : this.el.innerHTML
    this.suffix = null

    this.init()

    this.innerWidth = this.getStyle('width');
    (!this.options.useNative && this.options.responsive) && this.onResize()
  }

  init () {
    if (this.options.useNative && typeof this.el.style.webkitLineClamp !== 'undefined') {
      this.processNative()
      return
    }

    if (this.options.clamp === 'auto') {
      this.clampValue = this.getMaxLines()
      this.el.style.height = 'auto'
    }

    if (this.isFixedHeight) {
      this.clampValue = this.getMaxLines(parseInt(this.clampValue))
    }

    this.process()
  }

  reset () {
    this.suffix && this.suffix.remove()
    this.suffix = null
    this.node.nodeValue = this.original
    this.init()
  }

  onResize () {
    if (this.collapse) return
    this.destroy()
    this.el.resize = debounce(() => {
      if (this.innerWidth === this.getStyle('width')) return
      this.innerWidth = this.getStyle('width')
      this.reset()
    })
    addListener(this.el, this.el.resize)
  }

  destroy () {
    removeListener(this.el, this.el.resize)
    this.el.resize = null
  }

  process () {
    const maxHeight = this.getMaxHeight()
    this.suffix = this.el.querySelector('.clamp-suffix') || document.createElement('span')
    !this.suffix.classList.contains('clamp-suffix') && this.suffix.classList.add('clamp-suffix')
    this.suffix.style.cursor = 'pointer'

    if (this.options.collapse) {
      this.suffix.innerHTML = this.options.collapseBeforeSuffixHTML ? this.options.collapseBeforeSuffixHTML : '... 展开'
    } else {
      this.suffix.innerHTML = this.options.suffixHTML ? this.options.suffixHTML : '...'
    }

    this.suffix.onclick = () => {
      if (this.options.collapse) {
        this.collapse = !this.collapse
        if (this.collapse) {
          this.node.nodeValue = this.original
          this.suffix.innerHTML = this.options.collapseAfterSuffixHTML ? this.options.collapseAfterSuffixHTML : '... 收起'
        } else {
          this.suffix.innerHTML = this.options.collapseBeforeSuffixHTML ? this.options.collapseBeforeSuffixHTML : '... 展开'
          this.handle(maxHeight)
        }
      } else {
        typeof this.options.suffixClick === 'function' && this.options.suffixClick(this.original)
      }
    }
    !this.el.contains(this.suffix) && this.el.appendChild(this.suffix)
    this.handle(maxHeight)
  }

  handle (maxHeight) {
    if (maxHeight < this.el.clientHeight && this.node && this.node.nodeType === 3) {
      this.truncate(this.node, maxHeight)
    } else {
      this.el.removeChild(this.suffix)
      this.suffix = null
    }
  }

  truncate (node, maxHeight) {
    let chunks = node.nodeValue
    let size = Math.ceil(node.nodeValue.length / 10)
    let lastChunks = null
    while (chunks.length) {
      lastChunks = chunks
      chunks = chunks.slice(0, chunks.length - size)
      node.nodeValue = chunks
      if (this.el.clientHeight <= maxHeight) {
        if (size !== 1) {
          size = 1
          chunks = lastChunks
        } else {
          return
        }
      }
    }
  }

  getMaxLines (fixedHeight) {
    const availHeight = fixedHeight || this.el.clientHeight
    const lineHeight = this.getLineHeight()
    return Math.max(Math.floor(availHeight / lineHeight), 0)
  }

  getLineHeight () {
    let lineHeight = this.getStyle('line-height')
    if (lineHeight === 'normal') {
      lineHeight = parseInt(this.getStyle('font-size')) * 1.2
    }
    return parseInt(lineHeight)
  }

  getMaxHeight () {
    const lineHeight = this.getLineHeight()
    return lineHeight * this.clampValue
  }

  processNative () {
    Object.assign(this.el.style, {
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      webkitBoxOrient: 'vertical',
      display: '-webkit-box'
    })
    if (this.isFixedHeight) {
      Object.assign(this.el.style, {
        webkitLineClamp: this.getMaxLines(parseInt(this.clampValue)),
        height: this.options.clamp
      })
    } else {
      Object.assign(this.el.style, {
        webkitLineClamp: this.clampValue
      })
    }
  }

  getStyle (prop) {
    return (this.el.currentStyle || window.getComputedStyle(this.el, null))[prop]
  }
}

export function createClamp (el, params) {
  const options = {}
  typeof params === 'number' ? (options.clamp = params) : Object.assign(options, params)
  return new Clamp(el, options)
}
