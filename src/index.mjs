import clampjs from '../dist/clamp.cjs.js'

const { createClamp } = clampjs

export {
  clampjs as default,
  createClamp
}
