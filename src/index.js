import {createClamp} from "./clamp-class";


export default {
    version: '__VERSION__',
    createClamp
}

export {
    createClamp
}
