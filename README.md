<h2 align="center">@penglei188288/clampjs</h2>
<p align="center"><b>@penglei188288/clampjs 能够在HTML元素内容过长时在后面添加省略标志。提供原生-webkit-line-clamp和js实现两种实现方式。</b</p>

## 安装

```shell
npm install @penglei188288/clampjs --save
# or
yarn add @penglei188288/clampjs
```

## 示例

```javascript
import { createClamp } from '@penglei188288/clampjs'

const example = createClamp(document.getElementById('app'), {
    clamp: 2,
    useNative: false,
    suffixHTML: '<span class="text-primary m-l-5">...查看全部</span>',
    responsive: false,
    suffixClick: (original) => {
        alert(original)
    }
})

example.reset() //刷新
example.destroy() // responsive为true时会监听resize事件，调用destroy()函数，会清除监听的事件。以免造成性能问题

```

## 参数
| 参数  | 说明  | 类型  | 可选值  | 默认值  |
| ------------ | ------------ | ------------ | ------------ | ------------ |
| clamp  | 这个选项控制何时何地去限制文本元素。第一，当选项为数字的时候，则代表应该被显示的行数。第二，你可以使用代表 CSS的值（px,em）字符串来控制元素显示的高度。最后，你可以使用’auto’字符串。’auto’将会尝试铺满有效的空白区域并且自动的限制元素使其自适应。  |  number/string  |  —  | 2  |
| useNative  | 是否使用原生的 -webkit-line-clamp 属性在 支持的浏览器中。 默认是true 。如果你使用的是 Webkit 内核的浏览器，但是在某些情况下，显示不正常。你可以把这个值设置为false，使用基于js的实现方式  | boolean   | true  | false  |
| responsive  | 是否监听窗口变化以触发刷新，useNative为true时不会监听。使用后注意调用实例的destroy ()函数销毁，以免出现性能问题  |  boolean | true  | false  |
| suffixHTML  | 在HTML元素截断之后显示的字符串，默认是省略号(…)。同时支持 字符串和HTML标签。useNative为false时生效  | string   |  —  |  ...  |
| suffixClick  | 点击省略号触发时的回调函数，回调的参数为original。useNative为false时生效  | Function(original)  | —  |  — |

## 维护者
- [penglei](https://www.penglei.cc/)

## License
- [MIT](https://opensource.org/licenses/MIT)
